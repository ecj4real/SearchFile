﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace SearchFile
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            
            string path = "C:/Users/ECJ4REAL.ECJ4REAL-PC/Desktop/assignment.txt";   //path to the file
            Console.Write("Please enter search word: ");
            string search = Console.ReadLine();         // word being searched for in the file

            timer.Start();

            bool isFound = isWordFound(path, search);   // test to show if file is found

            if (isFound)
            {
                Console.WriteLine("\nFound");
            }
            else
            {
                Console.WriteLine("\nDoes not exist");
            }
            timer.Stop();
            Console.WriteLine("Time taken: {0}", timer.Elapsed);
            Console.WriteLine("\n\nPress Any key to continue . . .");
            Console.ReadKey();
        }

        public static bool isWordFound(string path, string search)
        {
            String document = "";   //to store string output of the file
            try
            {
                FileStream F = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);    // Filestream to read file

                byte[] bytes = new byte[F.Length];      //initialize byte array to store byte equivalent of each character in the file
                int numBytesToRead = (int)F.Length;     // number of bytes to read from filestream
                int numBytesRead = 0;
                
                while(numBytesToRead > 0)
                {
                    int n = F.Read(bytes, numBytesRead, numBytesToRead); //reads a byte from the filestream

                    if(n == 0)
                    {
                        break;
                    }

                    numBytesRead += n;
                    numBytesToRead -= n;
                }
                document = Encoding.ASCII.GetString(bytes); // converts the bytes array to its string equivalent
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            

            for(int i = 0; i < document.Length; i++)
            {
                /*
                if (word.Equals(search, StringComparison.InvariantCultureIgnoreCase))   //Ignores cases of the search word in the file
                {
                    return true;
                }
                */
                string s = "";

                while (i < document.Length)
                {
                    if(document[i] == ' ')
                    {
                        break;
                    }
                    s = s + document[i];
                    i++;
                }
               
                if (s == search)
                {
                    return true;
                }

            }
            return false;
        }

    }
}
